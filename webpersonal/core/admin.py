from django.contrib import admin
from .models import Contacto
from django import forms
# Register your models here.

class ContactoAdminForm(forms.ModelForm):

    class Meta:
        model = Contacto
        fields = '__all__'


class ContactoAdmin(admin.ModelAdmin):
    form = ContactoAdminForm
    list_display = ['name', 'phone', 'correo', 'asunto','created', 'updated']
    readonly_fields = ['name', 'phone', 'correo', 'asunto','created', 'updated']

admin.site.register(Contacto, ContactoAdmin)