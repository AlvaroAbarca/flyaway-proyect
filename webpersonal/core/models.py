from django.db import models

# Create your models here.
class Contacto(models.Model):
	name = models.CharField(max_length=30, verbose_name="Nombre")
	phone = models.CharField(max_length=30, verbose_name="Telefono")
	correo = models.EmailField(verbose_name="Correo")
	asunto = models.TextField(verbose_name="Asunto")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación", editable= False)
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición", editable = False)

	class Meta:
		ordering = ["-created"]

	def __str__(self):
		return self.name


