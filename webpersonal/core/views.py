from django.shortcuts import render, HttpResponse
from core.forms import ContactoForm
# Create your views here.
def home(request):
    return render(request, "core/home.html")

def about(request):
    return render(request, "core/about.html")

def contact(request):
	data = {}
	if request.method == "POST":
		data['form'] = ContactoForm(request.POST)
		if data['form'].is_valid():
			data ['form'].save()
	else:
			data['form'] = ContactoForm()
	return render(request, "core/contact.html",data)

